﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    namespace SimpleClassLibrary
    {

        public class ZNO
        {
            public string Subject { get; set; }
            public int Points { get; set; }

            public ZNO(string subject, int points)
            {
                Subject = subject;
                Points = points;
            }
        }
    }

